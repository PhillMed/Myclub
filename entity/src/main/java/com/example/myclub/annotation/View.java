package com.example.myclub.annotation;

import org.hibernate.annotations.Immutable;

@Immutable
public @interface View {
}

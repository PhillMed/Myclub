package com.example.myclub.converter;

import com.example.myclub.entity.Role;
import com.example.myclub.entity.VipType;
import org.springframework.stereotype.Component;

import javax.persistence.AttributeConverter;

@Component
public class VipTypeConverter implements AttributeConverter<VipType, String> {


    @Override
    public String convertToDatabaseColumn(VipType attribute) {
        return attribute.getValue();
    }

    @Override
    public VipType convertToEntityAttribute(String dbData) {
        return VipType.getByValue(dbData);
    }
}

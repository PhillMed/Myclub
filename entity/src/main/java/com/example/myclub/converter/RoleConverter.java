package com.example.myclub.converter;

import com.example.myclub.entity.Role;
import org.springframework.stereotype.Component;

import javax.persistence.AttributeConverter;

@Component
public class RoleConverter implements AttributeConverter<Role, String> {


    @Override
    public String convertToDatabaseColumn(Role attribute) {
        return attribute.getValue();
    }

    @Override
    public Role convertToEntityAttribute(String dbData) {
        return Role.getByValue(dbData);
    }
}

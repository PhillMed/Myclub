package com.example.myclub.entity;

import lombok.*;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@Table(name = "club_visit")
@Entity
public class Visit {

    @Id
    @GeneratedValue
    private UUID id;
    private int version;
    @Column(name = "visit_time")
    private OffsetDateTime time;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "visit_note_ID")
    private Notes notes;

    @OneToOne
    private Receipt receipt;

}

package com.example.myclub.entity;

import lombok.*;

import javax.persistence.Embeddable;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Embeddable
public class ContactDetails {

    private String phone;
    private String city;
    private String street;
    private String building;
}

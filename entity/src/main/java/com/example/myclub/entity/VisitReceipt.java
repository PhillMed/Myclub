package com.example.myclub.entity;

import com.example.myclub.annotation.View;
import lombok.Getter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Getter
@Entity
@View
@Table(name = "VISIT_RECEIPT")
public class VisitReceipt {

    @Id
    private Long id;

    @Column(name = "RECEIPT_ID")
    private Long receiptId;

    @Column(name = "TOTAL_AMOUNT")
    private BigDecimal totalAmount;

    @Column(name = "LOYALTY_CARD")
    private String loyaltyCard;

    @Column(name = "LOYALTY_PROGRAM")
    private String loyaltyProgram;
}

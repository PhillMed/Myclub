package com.example.myclub.entity.provision;

public enum NonAlcoholic {
    SODA, JUICE, OTHER
}
package com.example.myclub.entity;

import com.example.myclub.entity.provision.Provisions;
import lombok.*;

import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import java.io.Serializable;

@Setter
@Getter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Embeddable
public class ProductPK implements Serializable {

    @Embedded
    private Provisions provisions;
}

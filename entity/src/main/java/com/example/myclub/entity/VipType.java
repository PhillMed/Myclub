package com.example.myclub.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Getter
public enum VipType {
    SUPERSTAR(Values.SUPERSTAR),
    STAR(Values.STAR),
    FAMOUS(Values.FAMOUS);

    private static final Map<String, VipType> MAP = Arrays.stream(VipType.values())
            .collect(Collectors.toMap(VipType::getValue, Function.identity()));

    private final String value;

    public static VipType getByValue(String value) {
        if (Objects.isNull(value)) {
            return null;
        }
        return MAP.get(value);
    }

    public static class Values {
        public static final String SUPERSTAR = "SuperStar";
        public static final String STAR = "Star";
        public static final String FAMOUS = "Famous";
    }
}

package com.example.myclub.entity;

import lombok.*;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
@Entity
@Table(name = "UoM")
public class UnitOfMeasure {

    @Enumerated(EnumType.STRING)
    @Id
    private UOM code;
    private String codeIso;
    private String description;


}

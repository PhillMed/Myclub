package com.example.myclub.entity;

public enum UOM {
    KG, L, PACK, PINT, BOX
}

package com.example.myclub.entity.vips;

import com.example.myclub.entity.VipPersons;
import com.example.myclub.entity.VipType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@DiscriminatorValue(value = VipType.Values.SUPERSTAR)
@Entity
public class SuperStar extends VipPersons {

    @Column(name = VipPersons.FIRSTNAME)
    private String superStarFirstName;
    @Column(name = VipPersons.LASTNAME)
    private String superStarLastName;
}

package com.example.myclub.entity.provision;

import lombok.*;

import javax.persistence.Embeddable;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
@Embeddable
public class Drinks {

    public Alcoholic alcoholic;
    public NonAlcoholic nonAlcoholic;
}
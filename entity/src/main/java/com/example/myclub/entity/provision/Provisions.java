package com.example.myclub.entity.provision;

import lombok.*;

import javax.persistence.Embeddable;
import javax.persistence.Embedded;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
@Embeddable
public class Provisions {

    public Food food;

    @Embedded
    public Drinks drinks;
}
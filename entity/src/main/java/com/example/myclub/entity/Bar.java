package com.example.myclub.entity;

import lombok.*;

import javax.persistence.CascadeType;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.math.BigDecimal;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@Entity
public class Bar {

    @EmbeddedId
    private ProductPK productId;
    private String name;
    private BigDecimal price;

    @ManyToOne
    private UnitOfMeasure unitOfMeasure;

    @ManyToOne(cascade = CascadeType.PERSIST)
    private Receipt receipt;
}

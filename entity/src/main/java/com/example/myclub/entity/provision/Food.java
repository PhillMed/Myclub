package com.example.myclub.entity.provision;

public enum Food {
    SNACKS, MEAL, DESERTS
}
package com.example.myclub.entity.provision;

public enum Alcoholic {
    BEER, VINE, LIGHT, HEAVY
}
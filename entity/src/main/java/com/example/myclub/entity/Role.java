package com.example.myclub.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

@Getter
@RequiredArgsConstructor
public enum Role {

    OWNER("Owner"), MANAGER("Mgr"), BARMAN("Bar"), STAFF("Staff"), VISITOR("Visitor");

    private static final Map<String, Role> MAP = Arrays.stream(Role.values())
            .collect(Collectors.toMap(Role::getValue, Function.identity()));

    private final String value;

    public static Role getByValue(String value) {
        if (Objects.isNull(value)) {
            throw new RuntimeException();
        }

        Role role = MAP.get(value);
        return Objects.isNull(role) ? VISITOR : role;
    }
}

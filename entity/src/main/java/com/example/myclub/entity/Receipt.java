package com.example.myclub.entity;

import lombok.*;
import org.apache.commons.collections4.CollectionUtils;

import javax.persistence.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
@Entity
public class Receipt {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private BigDecimal amount;

    @Transient
    private BigDecimal amountInUsd;

    @OneToMany(mappedBy = "receipt", cascade = CascadeType.ALL)
    private List<Bar> buying;

    @OneToOne
    private Visit visit;

    @Builder.Default
    @Enumerated(EnumType.STRING)
    private ReceiptStatus status = ReceiptStatus.PENDING;

    @PrePersist
    @PreUpdate
    private void calculateAmount() {
        BigDecimal amount = BigDecimal.ZERO;
        if (CollectionUtils.isNotEmpty(buying)) {
            for (Bar buying : buying) {
                amount = amount.add(buying.getPrice());
            }
        }
        this.amount = amount;
    }

    @PostLoad
    @PostPersist
    @PostUpdate
    private void calcAmountInUsd() {
        this.amountInUsd = amount.divide(BigDecimal.valueOf(75.54), 2, RoundingMode.HALF_UP);
    }
}

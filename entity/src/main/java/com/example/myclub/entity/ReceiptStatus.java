package com.example.myclub.entity;

public enum ReceiptStatus {

    PENDING, PAYED, CANCELLED
}

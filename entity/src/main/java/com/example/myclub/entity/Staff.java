package com.example.myclub.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.*;
import java.time.LocalDate;

@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@Getter
@Setter
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Staff extends Individual {

    @Id
    @GeneratedValue
    private Long id;
    private LocalDate birthDate;
    private StaffSpecification specification;
    private String phone;
    private String salary;
    private String schedule;
    private String description;
}

package com.example.myclub.entity;

import com.example.myclub.converter.VipTypeConverter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.time.LocalDate;

@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@Getter
@Setter
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "vip_type", discriminatorType = DiscriminatorType.STRING)
@Entity
public class VipPersons extends BaseEntity<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private LocalDate birthDate;

    @Column(name = "vip_type", insertable = false, updatable = false)
    @Convert(converter = VipTypeConverter.class)
    private VipType vipType;

    public static final String FIRSTNAME = "first_name";
    public static final String LASTNAME = "last_name";
    
}

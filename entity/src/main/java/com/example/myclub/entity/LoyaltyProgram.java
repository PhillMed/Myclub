package com.example.myclub.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;

@Setter
@Getter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@PrimaryKeyJoinColumn(name = "owner_id")
@Entity
@Table(name = "LOYALTY_PROGRAM")
public class LoyaltyProgram extends Owner{

    @Column(name = "LOYALTY_CARD")
    private String loyaltyCard;

    @Column(name = "LOYALTY_DISCOUNT")
    private Long discount;

    @Column(name = "LOYALTY_AMOUNT")
    private Long loyaltyAmount;

    @Column(name = "LOYALTY_LVL")
    private Long loyaltyLvl;

}

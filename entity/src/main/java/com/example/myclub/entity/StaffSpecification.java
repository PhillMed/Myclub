package com.example.myclub.entity;

public enum StaffSpecification {
    BARMAN, CLEANER, SECURITY, MANAGER
}

package com.example.system.entity;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "SYSTEM_OPTION")
public class SystemOptionEntity {

    @Id
    @Column(name = "ID")
    private String id;

    @Column(name = "VALUE")
    private String value;
}

package com.example.myclub.specification.filter;

import com.example.myclub.entity.Owner;
import com.example.myclub.entity.Owner_;
import com.example.myclub.specification.OwnerSpecification;
import lombok.Builder;
import lombok.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import java.util.Objects;

@Builder
@Value
public class OwnerFilter implements Filter<Owner> {

    private static final Sort DEFAULT_SORTING = Sort.by(Sort.Direction.DESC, Owner_.CREATED_AT);

    Integer pageNumber;
    String term;

    String firsName;
    String lastName;
    boolean isActive;

    @Override
    public Pageable getPageable() {
        int page = Objects.isNull(pageNumber) ? 0 : pageNumber - 1;
        return PageRequest.of(page, DEFAULT_PAGE_SIZE, DEFAULT_SORTING);
    }

    @Override
    public Specification<Owner> getSpecification() {
        return OwnerSpecification.builder().filter(this).build();
    }
}

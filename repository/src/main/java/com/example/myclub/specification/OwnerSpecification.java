package com.example.myclub.specification;

import com.example.myclub.entity.ContactDetails_;
import com.example.myclub.entity.Owner;
import com.example.myclub.entity.Owner_;
import com.example.myclub.specification.filter.OwnerFilter;
import com.example.myclub.specification.filter.SpecBuilder;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

public interface OwnerSpecification extends Specification<Owner> {

    static OwnerSpecification hasFirstName(String value) {
        return ((root, query, criteriaBuilder) -> criteriaBuilder.equal(criteriaBuilder.lower(root.get(Owner_.FIRST_NAME)), value.toLowerCase()));
    }

    static OwnerSpecification firstNameLike(String value) {
        return ((root, query, criteriaBuilder) -> criteriaBuilder.like(criteriaBuilder.lower(root.get(Owner_.FIRST_NAME)),
                "%" + value.toLowerCase() + "%"));
    }

    static OwnerSpecification hasLastName(String value) {
        return ((root, query, criteriaBuilder) -> criteriaBuilder.equal(criteriaBuilder.lower(root.get(Owner_.LAST_NAME)), value.toLowerCase()));
    }

    static OwnerSpecification lastNameLike(String value) {
        return ((root, query, criteriaBuilder) -> criteriaBuilder.like(criteriaBuilder.lower(root.get(Owner_.LAST_NAME)),
                "%" + value.toLowerCase() + "%"));
    }

    static OwnerSpecification cityLike(String value) {
        return ((root, query, criteriaBuilder) -> criteriaBuilder.like(criteriaBuilder
                .lower(root.join(Owner_.CONTACT_DETAILS).get(ContactDetails_.CITY)), "%" + value.toLowerCase() + "%"));
    }

    static OwnerSpecification streetLike(String value) {
        return ((root, query, criteriaBuilder) -> criteriaBuilder.like(criteriaBuilder
                .lower(root.join(Owner_.CONTACT_DETAILS).get(ContactDetails_.STREET)),"%" + value.toLowerCase() + "%"));
    }

    static Specification<Owner> findByTerm(String term) {
        List<Specification<Owner>> specList = new ArrayList<>();
        specList.add(firstNameLike(term));
        specList.add(lastNameLike(term));
        specList.add(streetLike(term));
        specList.add(cityLike(term));

        return SpecificationComposer.compose(specList, Predicate.BooleanOperator.OR);
    }
    
    static OwnerSpecification.Builder builder() {
        return new OwnerSpecification.Builder();
    }

    class Builder extends SpecBuilder<Owner, OwnerFilter> {

        @Override
        public Specification<Owner> build() {
            List<Specification<Owner>> specList = new ArrayList<>();

            if (StringUtils.isNotEmpty(filter.getFirsName())) {
                specList.add(hasFirstName(filter.getFirsName()));
            }

            if (StringUtils.isNotEmpty(filter.getLastName())) {
                specList.add(hasLastName(filter.getLastName()));
            }

            if (StringUtils.isNotEmpty(filter.getTerm())) {
                specList.add(findByTerm(filter.getTerm()));
            }

            return SpecificationComposer.compose(specList);
        }
    }

}

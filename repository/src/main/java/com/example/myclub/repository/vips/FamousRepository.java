package com.example.myclub.repository.vips;

import com.example.myclub.entity.vips.Famous;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FamousRepository extends JpaRepository<Famous, Long> {
}

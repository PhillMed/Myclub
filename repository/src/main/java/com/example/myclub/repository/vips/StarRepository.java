package com.example.myclub.repository.vips;

import com.example.myclub.entity.vips.Star;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StarRepository extends JpaRepository<Star, Long> {
}

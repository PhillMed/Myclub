package com.example.myclub.repository;

import com.example.myclub.entity.VisitReceipt;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface VisitReceiptRepository extends JpaRepository<VisitReceipt, Long> {

    List<VisitReceipt> findByLoyaltyProgramContainsIgnoreCase(String loyaltyProgram);
}

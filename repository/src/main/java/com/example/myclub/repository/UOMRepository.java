package com.example.myclub.repository;

import com.example.myclub.entity.UOM;
import com.example.myclub.entity.UnitOfMeasure;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UOMRepository extends JpaRepository<UnitOfMeasure, UOM> {
}

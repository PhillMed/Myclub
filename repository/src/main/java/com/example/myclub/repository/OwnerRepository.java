package com.example.myclub.repository;

import com.example.myclub.entity.Owner;
import com.example.myclub.specification.SearchableRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface OwnerRepository extends IndividualRepository<Owner, Long>, SearchableRepository<Owner, Long> {

    List<Owner> findByCreatedByAndLastName(String createdBy, String lastName);

    List<Owner> findByLastNameOrFirstName(String lastName, String fistName);

    List<Owner> findByLastNameIgnoreCase(String lastName);

    List<Owner> findByLastNameOrFirstNameAllIgnoreCase(String lastName, String firstName);

    List<Owner> findByLastNameContaining(String name);

    List<Owner> findByLastNameLike(String name);

    List<Owner> findByLastNameAndContactDetailsPhone(String lastName, String phone);

}

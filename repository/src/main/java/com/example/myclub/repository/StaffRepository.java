package com.example.myclub.repository;

import com.example.myclub.entity.Staff;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface StaffRepository extends IndividualRepository<Staff, Long> {

    List<Staff>findByLastNameAndFirstName(String lastName, String FirstName);

    @Query(value = "SELECT s FROM Staff s where s.firstName like :staffName", nativeQuery = true)
    List<Staff> findByLastName(@Param("staffName") String name);
}

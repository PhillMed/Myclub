package com.example.myclub.repository;

import com.example.myclub.entity.Individual;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;

@NoRepositoryBean
public interface IndividualRepository<T extends Individual, ID> extends JpaRepository<T, ID> {

    List<T> findByFirstName(String firstName);
    List<T> findByLastName(String lastName);
    List<T> findByFullName(String fullName);
}

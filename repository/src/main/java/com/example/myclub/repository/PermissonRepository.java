package com.example.myclub.repository;

import com.example.myclub.entity.Permission;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PermissonRepository extends JpaRepository<Permission, Long> {

    Optional<Permission> findByName(String name);
}

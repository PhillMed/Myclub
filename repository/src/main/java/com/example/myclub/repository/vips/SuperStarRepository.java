package com.example.myclub.repository.vips;

import com.example.myclub.entity.vips.SuperStar;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SuperStarRepository extends JpaRepository<SuperStar, Long> {
}

package com.example.myclub.repository;

import com.example.myclub.entity.LoyaltyProgram;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface LoyaltyProgramRepository extends JpaRepository<LoyaltyProgram, Long> {

    List<LoyaltyProgram> findByLoyaltyCardAndLoyaltyLvl(String loyaltyCard, Long loyaltyLvl);
}

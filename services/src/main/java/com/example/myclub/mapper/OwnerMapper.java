package com.example.myclub.mapper;

import com.example.myclub.dto.OwnerDto;
import com.example.myclub.entity.Owner;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;

@Component
@Mapper
public interface OwnerMapper {

    @Mapping(target = "contactDetails", source = "dto")
    Owner map(OwnerDto dto);

    OwnerDto mapToDto(Owner entity);
}

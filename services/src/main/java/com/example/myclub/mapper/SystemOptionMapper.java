package com.example.myclub.mapper;

import com.example.myclub.dto.SystemOptionDto;
import com.example.system.entity.SystemOptionEntity;
import org.mapstruct.Mapper;

@Mapper
public interface SystemOptionMapper {

    SystemOptionDto mapToDto(SystemOptionEntity entity);
}

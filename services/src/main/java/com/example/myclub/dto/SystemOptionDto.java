package com.example.myclub.dto;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;

@Data
public class SystemOptionDto {

    private String id;
    private String value;
}

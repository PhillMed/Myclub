package com.example.myclub.dto;

import com.example.myclub.entity.ContactDetails;
import lombok.Data;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.LastModifiedBy;

import javax.persistence.Column;
import java.time.OffsetDateTime;

@Data
public class OwnerDto {

    private Long id;
    private String phone;
    private String city;
    private String street;
    private String building;
    private String firstName;
    private String lastName;
    private String fullName;

    public OffsetDateTime createdAt;
    private String createdBy;
    public OffsetDateTime lastModifiedAt;
    private String lastModifiedBy;
}

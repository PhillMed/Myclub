package com.example.myclub.services.map;

import com.example.myclub.entity.Owner;
import com.example.myclub.services.PersonsService;
import com.example.myclub.services.config.MapImplementation;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@MapImplementation
public class PersonsMapService extends AbstractMapService<Owner, Long> implements PersonsService {

    private static final Map<Long, Owner> resource = new HashMap<>();

    @Override
    public Map<Long, Owner> getResource() {
        return resource;
    }

    @Override
    public Collection<Owner> findByName(String name) {
        return null;
    }
}

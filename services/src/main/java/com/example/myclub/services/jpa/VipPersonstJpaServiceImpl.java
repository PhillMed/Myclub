package com.example.myclub.services.jpa;

import com.example.myclub.entity.VipPersons;
import com.example.myclub.services.VipPersonsService;
import com.example.myclub.services.config.JpaImplementation;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;

@JpaImplementation
public class VipPersonstJpaServiceImpl extends AbstractJpaService<VipPersons, Long> implements VipPersonsService {

    @Override
    public JpaRepository<VipPersons, Long> getRepository() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Collection<VipPersons> findByName(String name) {
        throw new UnsupportedOperationException();
    }
}

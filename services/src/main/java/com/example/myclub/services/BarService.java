package com.example.myclub.services;

import com.example.myclub.entity.Bar;

import java.util.Collection;

public interface BarService extends CrudService<Bar, Long> {

    Collection<Bar> findByProvType(String provType);
}

package com.example.myclub.services.impl;

import com.example.myclub.dto.SystemLogDto;
import com.example.myclub.mapper.SystemLogMapper;
import com.example.myclub.services.SystemLogService;
import com.example.system.repository.SystemLogRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class SystemLogServiceImpl implements SystemLogService {

    private final SystemLogRepository systemLogRepository;
    private final SystemLogMapper mapper;

    @Override
    public List<SystemLogDto> getAll() {
        return mapper.mapListToDto(systemLogRepository.findAll());
    }
}

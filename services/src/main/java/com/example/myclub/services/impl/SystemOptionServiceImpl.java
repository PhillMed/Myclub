package com.example.myclub.services.impl;

import com.example.myclub.dto.SystemOptionDto;
import com.example.myclub.mapper.SystemOptionMapper;
import com.example.myclub.services.SystemOptionService;
import com.example.system.repository.SystemRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class SystemOptionServiceImpl implements SystemOptionService {

    private final SystemRepository systemRepository;
    private final SystemOptionMapper systemOptionMapper;

    @Override
    public List<SystemOptionDto> getAll() {
        return systemRepository.findAll().stream().map(systemOptionMapper::mapToDto).collect(Collectors.toList());
    }
}

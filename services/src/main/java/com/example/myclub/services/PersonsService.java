package com.example.myclub.services;

import com.example.myclub.dto.OwnerDto;
import com.example.myclub.entity.Owner;
import com.example.myclub.specification.filter.OwnerFilter;
import org.springframework.data.domain.Page;

import java.util.Collection;
import java.util.List;

public interface PersonsService extends CrudService<Owner, Long> {

    Collection<Owner> findByName(String name);

    default List<OwnerDto> findOwner(OwnerDto ownerDto) {
        throw new UnsupportedOperationException();
    }

    default Page<OwnerDto> search(OwnerFilter ownerFilter) {
        throw new UnsupportedOperationException();
    };
}

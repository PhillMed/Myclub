package com.example.myclub.services.map;

import com.example.myclub.entity.VipPersons;
import com.example.myclub.services.VipPersonsService;
import com.example.myclub.services.config.MapImplementation;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@MapImplementation
public class VipPersonsMapService extends AbstractMapService<VipPersons, Long> implements VipPersonsService {

    private static final Map<Long, VipPersons> resource = new HashMap<>();

    @Override
    public Map<Long, VipPersons> getResource() {
        return resource;
    }

    @Override
    public Collection<VipPersons> findByName(String name) {
        return null;
    }
}

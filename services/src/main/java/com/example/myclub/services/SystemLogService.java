package com.example.myclub.services;

import com.example.myclub.dto.SystemLogDto;

import java.util.List;

public interface SystemLogService {

    List<SystemLogDto> getAll();
}

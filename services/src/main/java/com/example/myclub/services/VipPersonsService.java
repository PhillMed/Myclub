package com.example.myclub.services;

import com.example.myclub.entity.VipPersons;

import java.util.Collection;

public interface VipPersonsService extends CrudService<VipPersons, Long> {

    Collection<VipPersons> findByName(String name);
}

package com.example.myclub.services.jpa;

import com.example.myclub.entity.Staff;
import com.example.myclub.repository.StaffRepository;
import com.example.myclub.services.StaffService;
import com.example.myclub.services.config.JpaImplementation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

@RequiredArgsConstructor
@Transactional(readOnly = true)
@JpaImplementation
public class StaffJpaServiceImpl extends AbstractJpaService<Staff, Long> implements StaffService {

    private final StaffRepository staffRepository;

    @Override
    public JpaRepository<Staff, Long> getRepository() {
        return staffRepository;
    }

    @Override
    public Collection<Staff> findByName(String name) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Collection<Staff> findBySpec(String spec) {
        throw new UnsupportedOperationException();
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void updatePhoneNumber(Long staffId, String phone) {
            Staff staff = staffRepository.findById(staffId).orElseThrow();
            staff.setPhone(phone);
    }
}

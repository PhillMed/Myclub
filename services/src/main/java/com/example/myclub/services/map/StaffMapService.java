package com.example.myclub.services.map;

import com.example.myclub.entity.Staff;
import com.example.myclub.services.StaffService;
import com.example.myclub.services.config.MapImplementation;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@MapImplementation
public class StaffMapService extends AbstractMapService<Staff, Long> implements StaffService {

    private static final Map<Long, Staff> resource = new HashMap<>();

    @Override
    public Map<Long, Staff> getResource() {
        return resource;
    }

    @Override
    public Collection<Staff> findByName(String name) {
        return null;
    }

    @Override
    public Collection<Staff> findBySpec(String spec) {
        return null;
    }

    @Override
    public void updatePhoneNumber(Long staffId, String phone) {
        throw new UnsupportedOperationException();
    }
}

package com.example.myclub.services;

import com.example.myclub.entity.Staff;

import java.util.Collection;

public interface StaffService extends CrudService<Staff, Long> {

    Collection<Staff> findByName(String name);

    Collection<Staff> findBySpec(String spec);

    void updatePhoneNumber(Long staffId, String phone);
}

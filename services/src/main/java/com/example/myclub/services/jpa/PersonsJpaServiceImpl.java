package com.example.myclub.services.jpa;

import com.example.myclub.dto.OwnerDto;
import com.example.myclub.entity.Owner;
import com.example.myclub.entity.Staff;
import com.example.myclub.mapper.OwnerMapper;
import com.example.myclub.repository.OwnerRepository;
import com.example.myclub.repository.StaffRepository;
import com.example.myclub.services.PersonsService;
import com.example.myclub.services.StaffService;
import com.example.myclub.services.config.JpaImplementation;
import com.example.myclub.specification.OwnerSpecification;
import com.example.myclub.specification.SearchableRepository;
import com.example.myclub.specification.SearchableService;
import com.example.myclub.specification.filter.OwnerFilter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@Transactional(readOnly = true)
@JpaImplementation
public class PersonsJpaServiceImpl extends AbstractJpaService<Owner, Long> implements PersonsService, SearchableService<Owner> {

    private final StaffRepository staffRepository;
    private final StaffService staffService;
    private final OwnerMapper mapper;
    private final OwnerRepository ownerRepository;

    @Override
    public JpaRepository<Owner, Long> getRepository() {
        throw new UnsupportedOperationException();
    }

    @Override
    public SearchableRepository<Owner, ?> getSearchRepository() {
        return ownerRepository;
    }

    @Override
    public Collection<Owner> findByName(String name) {
        throw new UnsupportedOperationException();
    }

    public void readStaffAndPhone() {
        List<Staff> staff = staffRepository.findAll();
        for (Staff staff1 : staff) {
            log.warn("Staff name {}", staff1.getFullName());
            log.warn("Staff phone {}", staff1.getPhone());
        }
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    public void updateStaffPhone(Long id, String lastName, String staffPhone) {
        Staff staff = staffRepository.findById(id).orElseThrow();
        staff.setLastName(lastName);

        if (CollectionUtils.isNotEmpty(Collections.singleton(staffPhone))) {
            log.info("Previous staff phone: {}", staff.getPhone());

            try {

                staffService.updatePhoneNumber(staff.getId(), staffPhone);
            } catch (Exception ex) {
                throw new RuntimeException();
            }
        }

    }


    @Override
    public List<OwnerDto> findOwner(OwnerDto ownerDto) {

        ExampleMatcher caseInsensitiveExMatcher = ExampleMatcher.matchingAll().withIgnoreCase();
        Example<Owner> example = Example.of(mapper.map(ownerDto), caseInsensitiveExMatcher);
        return ownerRepository.findAll(example).stream().map(mapper::mapToDto).collect(Collectors.toList());
    }

    @Override
    public Page<OwnerDto> search(OwnerFilter ownerFilter) {
        return searchPage(ownerFilter).map(mapper::mapToDto);
    }

}

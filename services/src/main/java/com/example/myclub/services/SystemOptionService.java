package com.example.myclub.services;

import com.example.myclub.dto.SystemOptionDto;

import java.util.List;

public interface SystemOptionService {

    List<SystemOptionDto> getAll();
}

create table BARMAN
(
    ID BIGINT not null
        primary key,
    CREATED_AT TIMESTAMP not null,
    CREATED_BY VARCHAR(255),
    LAST_MODIFIED_AT TIMESTAMP not null,
    LAST_MODIFIED_BY VARCHAR(255),
    FIRST_NAME VARCHAR(255),
    FULL_NAME VARCHAR(255),
    LAST_NAME VARCHAR(255),
    BIRTH_DATE DATE,
    SPECIFICATION VARCHAR(255),
    BARMAN_DESCRIPTION VARCHAR(255)
);

create table CLEANER
(
    ID BIGINT not null
        primary key,
    CREATED_AT TIMESTAMP not null,
    CREATED_BY VARCHAR(255),
    LAST_MODIFIED_AT TIMESTAMP not null,
    LAST_MODIFIED_BY VARCHAR(255),
    FIRST_NAME VARCHAR(255),
    FULL_NAME VARCHAR(255),
    LAST_NAME VARCHAR(255),
    BIRTH_DATE DATE,
    SPECIFICATION VARCHAR(255),
    CLEANER_DESCRIPTION VARCHAR(255)
);

create table FILIAL
(
    ID BIGINT auto_increment
        primary key,
    CITY VARCHAR(255),
    FILIAL_NAME VARCHAR(255)
);

create table MANAGER
(
    ID BIGINT not null
        primary key,
    CREATED_AT TIMESTAMP not null,
    CREATED_BY VARCHAR(255),
    LAST_MODIFIED_AT TIMESTAMP not null,
    LAST_MODIFIED_BY VARCHAR(255),
    FIRST_NAME VARCHAR(255),
    FULL_NAME VARCHAR(255),
    LAST_NAME VARCHAR(255),
    BIRTH_DATE DATE,
    SPECIFICATION VARCHAR(255),
    MANAGER_DESCRIPTION VARCHAR(255)
);

create table NOTES
(
    ID BIGINT auto_increment
        primary key,
    DESCRIPTION VARCHAR(255)
);

create table CLUB_VIP_VISIT
(
    ID BINARY not null
        primary key,
    VIP_VISIT_TIME TIMESTAMP,
    VERSION INTEGER not null,
    VISIT_NOTE_ID BIGINT,
    constraint FK9WPXLTKDBB209MG1FECGVCUSV
        foreign key (VISIT_NOTE_ID) references NOTES (ID)
);

create table OWNER
(
    OWNER_ID BIGINT auto_increment
        primary key,
    CREATED_AT TIMESTAMP not null,
    CREATED_BY VARCHAR(255),
    LAST_MODIFIED_AT TIMESTAMP not null,
    LAST_MODIFIED_BY VARCHAR(255),
    FIRST_NAME VARCHAR(255),
    FULL_NAME VARCHAR(255),
    LAST_NAME VARCHAR(255),
    PHONE VARCHAR(255)
);

create table PERMISSION
(
    ID BIGINT auto_increment
        primary key,
    NAME VARCHAR(255)
);

create table RECEIPT
(
    ID BIGINT auto_increment
        primary key,
    AMOUNT DECIMAL(19,2)
);

create table ITEM
(
    ID BIGINT auto_increment
        primary key,
    NAME VARCHAR(255),
    PRICE DECIMAL(19,2),
    RECEIPT_ID BIGINT,
    constraint FK8S305NSNV4WI5IUH7KKR52GQT
        foreign key (RECEIPT_ID) references RECEIPT (ID)
);

create table REGULAR_CLIENTS
(
    DISCOUNT BIGINT,
    LOYALTY_AMOUNT BIGINT,
    LOYALTY_CARD VARCHAR(255),
    LOYALTY_PROGRAM VARCHAR(255),
    OWNER_ID BIGINT not null
        primary key,
    constraint FK6YFALMFPRN4KX4O1Y9MGW166G
        foreign key (OWNER_ID) references OWNER (OWNER_ID)
);

create table SECURITY
(
    ID BIGINT not null
        primary key,
    CREATED_AT TIMESTAMP not null,
    CREATED_BY VARCHAR(255),
    LAST_MODIFIED_AT TIMESTAMP not null,
    LAST_MODIFIED_BY VARCHAR(255),
    FIRST_NAME VARCHAR(255),
    FULL_NAME VARCHAR(255),
    LAST_NAME VARCHAR(255),
    BIRTH_DATE DATE,
    SPECIFICATION VARCHAR(255),
    SECURITY_DESCRIPTION VARCHAR(255)
);

create table UOM
(
    CODE VARCHAR(255) not null
        primary key,
    CODE_ISO VARCHAR(255),
    DESCRIPTION VARCHAR(255)
);

create table BAR
(
    CODE VARCHAR(255) not null,
    CODE_PART VARCHAR(255) not null,
    NAME VARCHAR(255),
    PRICE DECIMAL(19,2),
    UNIT_OF_MEASURE_CODE VARCHAR(255),
    primary key (CODE, CODE_PART),
    constraint FKDG01NXOK140K9G32SHDPCV0S1
        foreign key (UNIT_OF_MEASURE_CODE) references UOM (CODE)
);

create table USER
(
    ID BIGINT auto_increment
        primary key,
    BUILDING2 VARCHAR(255),
    CITY2 VARCHAR(255),
    PHONE2 VARCHAR(255),
    STREET2 VARCHAR(255),
    BUILDING VARCHAR(255),
    CITY VARCHAR(255),
    PHONE VARCHAR(255),
    STREET VARCHAR(255),
    LOGIN VARCHAR(255),
    PASSWORD VARCHAR(255),
    ROLE VARCHAR(255)
);

create table USER_FILIALS
(
    USERS_ID BIGINT not null,
    FILIALS_ID BIGINT not null,
    primary key (USERS_ID, FILIALS_ID),
    constraint FK869IWDUHG3GRNN35RCKU1AMMA
        foreign key (USERS_ID) references USER (ID),
    constraint FKN6XTE0KCL15MDW4N5AWGYQ5S
        foreign key (FILIALS_ID) references FILIAL (ID)
);

create table USER_PERM
(
    USR_ID BIGINT not null,
    PERM_ID BIGINT not null,
    primary key (USR_ID, PERM_ID),
    constraint FK7UMITIFMVIGDD7JJXPLILEKY8
        foreign key (USR_ID) references USER (ID),
    constraint FKI197BA9K5L8A2A5HUTC95R3OY
        foreign key (PERM_ID) references PERMISSION (ID)
);

create table VIP_PERSONS
(
    VIP_TYPE VARCHAR(31) not null,
    ID BIGINT auto_increment
        primary key,
    CREATED_AT TIMESTAMP not null,
    CREATED_BY VARCHAR(255),
    LAST_MODIFIED_AT TIMESTAMP not null,
    LAST_MODIFIED_BY VARCHAR(255),
    BIRTH_DATE DATE,
    FIRST_NAME VARCHAR(255),
    LAST_NAME VARCHAR(255)
);


package com.example.myclub;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyclubApplication {

    public static void main(String[] args) {
        SpringApplication.run(MyclubApplication.class, args);
    }

}

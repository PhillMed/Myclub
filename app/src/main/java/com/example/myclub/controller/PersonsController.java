package com.example.myclub.controller;

import com.example.myclub.services.PersonsService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;

@RequiredArgsConstructor
@Controller
public class PersonsController {

    private final PersonsService personsService;
}

package com.example.myclub.controller;

import com.example.myclub.services.StaffService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;

@RequiredArgsConstructor
@Controller
public class StaffController {

    private final StaffService staffService;
}

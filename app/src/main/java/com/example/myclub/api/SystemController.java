package com.example.myclub.api;

import com.example.myclub.dto.SystemLogDto;
import com.example.myclub.dto.SystemOptionDto;
import com.example.myclub.services.SystemLogService;
import com.example.myclub.services.SystemOptionService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequiredArgsConstructor
@RequestMapping("/system")
@RestController
public class SystemController {

    private final SystemLogService systemLogService;
    private final SystemOptionService systemOptionService;


    @GetMapping("/logs")
    public List<SystemLogDto> getLogs() {
        return systemLogService.getAll();
    }

    @GetMapping("/options")
    public List<SystemOptionDto> getSystem() {
        return systemOptionService.getAll();
    }
}

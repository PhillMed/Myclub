package com.example.myclub.api;

import com.example.myclub.config.Runner;
import com.example.myclub.dto.OwnerDto;
import com.example.myclub.entity.Owner;
import com.example.myclub.entity.User;
import com.example.myclub.entity.VisitReceipt;
import com.example.myclub.repository.OwnerRepository;
import com.example.myclub.repository.VisitReceiptRepository;
import com.example.myclub.services.PersonsService;
import com.example.myclub.specification.filter.OwnerFilter;
import com.example.system.repository.SystemRepository;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.List;

@RequiredArgsConstructor
@RequestMapping("/")
@RestController
public class TestController {

    private final VisitReceiptRepository visitReceiptRepository;
    private final SystemRepository systemRepository;
    private final PersonsService ownerService;
    private final OwnerRepository ownerRepository;

    private final Runner runner;

    @GetMapping
    @Transactional
    public String test() {
        User user = runner.getUser();
        user.getPermissions().forEach(p -> System.out.println(p.getName()));
        return user.getLogin();
    }

    @GetMapping("/view")
    public List<VisitReceipt> getView() {
        return visitReceiptRepository.findByLoyaltyProgramContainsIgnoreCase("a");
    }

    @ApiOperation(value = "Find owners", response = Owner.class, responseContainer = "List")
    @PutMapping("/owners/find")
    public List<OwnerDto> findOwners(@ApiParam(value = "Search example") @RequestBody OwnerDto ownerDto) {
        return ownerService.findOwner(ownerDto);
    }

    @ApiOperation(value = "Search owners", response = Owner.class, responseContainer = "Page")
    @GetMapping("/owners/search")
    public Page<OwnerDto> search(@RequestParam(required = false) String term,
                                 @RequestParam(required = false) Integer pageNumber) {

        OwnerFilter ownerFilter = OwnerFilter.builder()
                .pageNumber(pageNumber)
                .term(term)
                .build();
        return ownerService.search(ownerFilter);
    }
}
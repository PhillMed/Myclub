package com.example.myclub.config;

import com.example.myclub.entity.StaffSpecification;
import com.example.myclub.entity.*;
import com.example.myclub.entity.provision.*;
import com.example.myclub.entity.vips.Famous;
import com.example.myclub.entity.vips.Star;
import com.example.myclub.entity.vips.SuperStar;
import com.example.myclub.repository.*;
import com.example.myclub.repository.vips.FamousRepository;
import com.example.myclub.repository.vips.StarRepository;
import com.example.myclub.repository.vips.SuperStarRepository;
import lombok.RequiredArgsConstructor;
import org.jasypt.encryption.StringEncryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;

@RequiredArgsConstructor
@Component("mainRunner")
public class Runner implements CommandLineRunner {

    private final VisitRepository visitRepository;
    private final NotesRepository notesRepository;
    private final BarRepository barRepository;
    private final ReceiptRepository receiptRepository;
    private final PermissonRepository permissonRepository;
    private final UserRepository userRepository;
    private final UOMRepository uomRepository;
    private final OwnerRepository ownerRepository;
    private final SuperStarRepository superStarRepository;
    private final StarRepository starRepository;
    private final FamousRepository famousRepository;
    private final LoyaltyProgramRepository loyaltyProgramRepository;
    private final StaffRepository staffRepository;


    @Qualifier("customEncryptor")
    @Autowired
    StringEncryptor stringEncryptor;
    @Value("${spring.datasource.password:undefined}")
    private String dbPwd;

    @Override
    public void run(String... args) throws Exception {
//        createUser();
//
//        createPersons();
//
//        encription();
//
//        createVisit();
//
//        createBar();
//
//        createReceipt();
//
//        createVIPs();
//
//        createRegularClients();
//
//        createStaff();

    }

    private void createStaff() {

        Staff staff1 = Staff.builder()
                .specification(StaffSpecification.BARMAN)
                .firstName("Alex")
                .lastName("Merlin")
                .birthDate(LocalDate.now())
                .phone("+7-123-456-78-90")
                .salary("35$/h")
                .schedule("2/3/4/5/6, 20:00-6:00")
                .build();

        Staff staff2 = Staff.builder()
                .specification(StaffSpecification.CLEANER)
                .firstName("Dmitriy")
                .lastName("Monday")
                .birthDate(LocalDate.now())
                .phone("+7-123-321-12-21")
                .salary("20$/h")
                .schedule("1-7, 10:00-18:00")
                .build();

        Staff staff3 = Staff.builder()
                .specification(StaffSpecification.SECURITY)
                .firstName("Vladimir")
                .lastName("Somatov")
                .birthDate(LocalDate.now())
                .phone("+7-111-222-33-44")
                .salary("25$/h")
                .schedule("2/3/4, 22:00-6:00")
                .build();

        Staff staff4 = Staff.builder()
                .specification(StaffSpecification.MANAGER)
                .firstName("Artem")
                .lastName("Smolyak")
                .birthDate(LocalDate.now())
                .phone("+7-222-444-66-88")
                .salary("60$/h")
                .schedule("1-7, 10:00-20:00")
                .build();


        staffRepository.saveAll(Arrays.asList(staff1, staff2, staff3, staff4));
    }

    private void createRegularClients() {
        LoyaltyProgram regularClient1 = LoyaltyProgram.builder()
                .firstName("Damir")
                .lastName("Timin")
//                .phone("+7-510-123-12-12")
                .loyaltyCard("1234-5678")
                .loyaltyAmount(500L)
                .loyaltyLvl(3L)
                .discount(20L)
                .build();
        loyaltyProgramRepository.save(regularClient1);
    }

    private void createVIPs() {
        SuperStar superStar = SuperStar.builder()
                .birthDate(LocalDate.now())
                .superStarFirstName("John")
                .superStarLastName("Depp")
                .build();

        superStarRepository.save(superStar);

        Star star = Star.builder()
                .birthDate(LocalDate.now())
                .starFirstName("Daniel")
                .starLastName("Radcliffe")
                .build();

        starRepository.save(star);

        Famous famous = Famous.builder()
                .birthDate(LocalDate.now())
                .famousFirstName("James")
                .famousLastName("Parsons")
                .build();

        famousRepository.save(famous);
    }

    private void createPersons() {
        Owner owner = Owner.builder()
                .firstName("Philip")
                .lastName("Medveshchek")
//                .phone("+7-977-111-22-33")
                .build();

        ownerRepository.save(owner);
    }

    @Transactional
    public User getUser() {
        User user = userRepository.findByLogin("user1").orElseThrow();
        System.out.println(user.getLogin());
        return user;
    }

    private void createUser() {

        Permission perm = permissonRepository.findByName("update_user")
                .orElseThrow(NoSuchElementException::new);

        Filial filial = Filial.builder()
                .filialName("filial1")
                .city("Minsk")
                .build();

        User user1 = User.builder()
                .login("user1")
                .password("12345678")
                .role(Role.OWNER)
                .permissions(Collections.singleton(perm))
                .filials(Collections.singleton(filial))
                .build();

        User user2 = User.builder()
                .login("user2")
                .password("333444555")
                .role(Role.MANAGER)
                .permissions(Collections.singleton(perm))
                .filials(Collections.singleton(filial))
                .build();

        userRepository.saveAll(Arrays.asList(user1, user2));
    }

    private void createReceipt() {
        Receipt receipt = Receipt.builder()
                .amount(BigDecimal.TEN)
                .build();
        Bar buying1 = Bar.builder()
                .name("coffee")
                .price(BigDecimal.valueOf(50))
                .productId(ProductPK.builder()
                        .provisions(Provisions.builder()
                                .drinks(Drinks.builder()
                                        .nonAlcoholic(NonAlcoholic.OTHER)
                                        .build())
                                .build())
                        .build())
                .receipt(receipt)
                .build();
        Bar buying2 = Bar.builder()
                .name("pizza")
                .price(BigDecimal.valueOf(400))
                .productId(ProductPK.builder()
                        .provisions(Provisions.builder()
                                .food(Food.MEAL)
                                .build())
                        .build())
                .receipt(receipt)
                .build();

        receipt.setBuying(Arrays.asList(buying1, buying2));

        receiptRepository.save(receipt);

        System.out.println("Amount in USD = " + receipt.getAmountInUsd());
    }

    private void createBar() {
        UnitOfMeasure liter = uomRepository.findById(UOM.L).orElseThrow();
        UnitOfMeasure pint = uomRepository.findById(UOM.PINT).orElseThrow();

        Bar bar1 = Bar.builder()
                .name("Pepsi")
                .price(BigDecimal.ONE)
                .productId(ProductPK.builder()
                        .provisions(Provisions.builder()
                                .drinks(Drinks.builder()
                                        .nonAlcoholic(NonAlcoholic.SODA)
                                        .build())
                                .build())
                        .build())
                .unitOfMeasure(liter)
                .build();

        Bar bar2 = Bar.builder()
                .name("Snow")
                .price(BigDecimal.TEN)
                .productId(ProductPK.builder()
                        .provisions(Provisions.builder()
                                .drinks(Drinks.builder()
                                        .alcoholic(Alcoholic.BEER)
                                        .build())
                                .build())
                        .build())
                .unitOfMeasure(pint)
                .build();

        barRepository.save(bar1);
        barRepository.save(bar2);
    }

    private void createVisit() {

        LoyaltyProgram regularClient1 = LoyaltyProgram.builder()
                .firstName("Damir")
                .lastName("Timin")
//                .phone("+7-510-123-12-12")
                .loyaltyCard("1234-5678")
                .loyaltyAmount(500L)
                .loyaltyLvl(3L)
                .discount(20L)
                .build();
        loyaltyProgramRepository.save(regularClient1);

        Notes notes1 = Notes.builder().description("desc1").build();
        Notes notes2 = Notes.builder().description("desc2").build();

        Receipt receipt1 = Receipt.builder()
                .amount(BigDecimal.ZERO)
                .build();

        Bar buying1r1 = Bar.builder()
                .name("coffee")
                .price(BigDecimal.valueOf(50))
                .receipt(receipt1)
                .build();
        Bar buying2r1 = Bar.builder()
                .name("pizza")
                .price(BigDecimal.valueOf(400))
                .receipt(receipt1)
                .build();
        Bar buying3r1 = Bar.builder()
                .name("potato")
                .price(BigDecimal.valueOf(150))
                .receipt(receipt1)
                .build();

        receipt1.setBuying(Arrays.asList(buying1r1, buying2r1, buying3r1));

        Receipt receipt2 = Receipt.builder()
                .amount(BigDecimal.ZERO)
                .build();

        Bar buying1r2 = Bar.builder()
                .name("coffee-big")
                .price(BigDecimal.valueOf(75))
                .receipt(receipt2)
                .build();

        receipt2.setBuying(List.of(buying1r2));
        receiptRepository.saveAll(Arrays.asList(receipt1, receipt2));

        Visit visit1 = Visit.builder().time(OffsetDateTime.now())
                .version(1)
                .notes(notes1)
                .receipt(receipt1)
                .build();
        Visit visit2 = Visit.builder().time(OffsetDateTime.now())
                .version(1)
                .notes(notes2)
                .receipt(receipt2)
                .build();
        visitRepository.saveAll(Arrays.asList(visit1, visit2));

        notes1.setDescription("desc1_updated");
        visit1.setVersion(2);
        visitRepository.save(visit1);

        visit2.setNotes(null);
        visitRepository.save(visit2);
    }

    private void encription() {
//        String pwd = "dev_pwd";
//        String encrypt = stringEncryptor.encrypt(pwd);
//        System.out.println(encrypt);
//
//        String decrypt = stringEncryptor.decrypt(encrypt);
//        System.out.println(decrypt);

        System.out.println(dbPwd);
    }
}
